package tron;

import processing.core.PApplet;
import processing.core.PImage;

public class Fenetre extends PApplet {

	static final int TAILLE_CASE = 15; // nombre de pixels pour la largeur/hauteur d'une case
	static final String AUTEURS = "Florian & Ivan"; // les auteurs
	final int BLEU = color(0, 0, 255);
	final int ROUGE = color(255, 0, 0);
	final int DEPLACEMENT = 15; // Vitesse de deplacement des joueurs
	public final int NB_CASES = 50;
	public final int TAILLE_FENETRE = NB_CASES * TAILLE_CASE;

	int directionJ1 = 1; // Direction dans laquelle les joueurs se deplacent
	int directionJ2 = 1;
	int gagnant = 0;
	int[][] etatCase = new int[NB_CASES][NB_CASES]; // Tableau de l'aire de jeu

	boolean commencer = false;

	Joueur j1 = new Joueur(this, 405, 375, TAILLE_CASE, TAILLE_CASE, BLEU, "Ivan");
	Joueur j2 = new Joueur(this, 345, 375, TAILLE_CASE, TAILLE_CASE, ROUGE, "Florian");

	// Chargement image
	PImage espace;

	public void settings() {
		size(TAILLE_FENETRE, TAILLE_FENETRE, P2D); // mode 2d

	}

	public void setup() {
		frameRate(30); // 30 images par seconde
		surface.setTitle("Mini-projet : Tron par " + AUTEURS); // titre de la fenêtre
		espace = loadImage("espace.jpg");
	}

	public void draw() {

		if (!commencer) {
			menu();
		} else {
			image(espace, 0, 0);
			j1.dessiner();
			j2.dessiner();
			controlesJ1();
			controlesJ2();
			dessinerCarte();
			changementEtatCarte();
		}

	}

	public void menu() {
		fill(0, 0, 0);
		rect(0, 0, TAILLE_FENETRE, TAILLE_FENETRE);
		textSize(30);
		textAlign(CENTER);
		fill(255);
		text("Pour lancer la partie, appuyer sur : r ", width / 2, height / 2);
	}

	public void keyPressed() {
		// Attribution des touches directionnelles de chaque joueurs
		switch (keyCode) {
		case UP:
			directionJ1 = 1;
			break;
		case DOWN:
			directionJ1 = 2;
			break;
		case LEFT:
			directionJ1 = 3;
			break;
		case RIGHT:
			directionJ1 = 4;
			break;
		}
		switch (key) {
		case 'z':
			directionJ2 = 1;
			break;
		case 's':
			directionJ2 = 2;
			break;
		case 'q':
			directionJ2 = 3;
			break;
		case 'd':
			directionJ2 = 4;
			break;
		case 'r':
			commencer = true;
			break;
		}
	}

	public void controlesJ1() {
		// Fonction pour la direction du joueur 1
		switch (directionJ1) {
		case 1:
			j1.posY -= DEPLACEMENT;
			break;
		case 2:
			j1.posY += DEPLACEMENT;
			break;
		case 3:
			j1.posX -= DEPLACEMENT;
			break;
		case 4:
			j1.posX += DEPLACEMENT;
			break;
		}

	}

	public void controlesJ2() {
		// Fonction pour la direction du joueur 2
		switch (directionJ2) {
		case 1:
			j2.posY -= DEPLACEMENT;
			break;
		case 2:
			j2.posY += DEPLACEMENT;
			break;
		case 3:
			j2.posX -= DEPLACEMENT;
			break;
		case 4:
			j2.posX += DEPLACEMENT;
			break;
		}

	}

	public void dessinerCarte() {
		// Dessine les cases de la carte en fonction du joueur qui l'a traversée
		for (int ligne = 0; ligne < NB_CASES; ligne++) {
			int y = floor(map(ligne, 0, NB_CASES, 0, TAILLE_FENETRE));
			for (int colonne = 0; colonne < NB_CASES; colonne++) {
				int x = floor(map(colonne, 0, NB_CASES, 0, TAILLE_FENETRE));
				switch (etatCase[ligne][colonne]) {
				case 0:
					fill(0, 0, 0, 0); // Etat neutre (affichage de l'img de fond)
					break;
				case 1:
					fill(BLEU);
					break;
				case 2:
					fill(ROUGE);
					break;
				}
				noStroke();
				rect(x, y, TAILLE_CASE, TAILLE_CASE);
			}
		}
	}

	public void changementEtatCarte() {
		// On défini sur quelle case se trouve le joueur 1
		int ligneJ1 = floor(map(j1.posY + (TAILLE_CASE / 2), 0, TAILLE_FENETRE, 0, NB_CASES));
		int colonneJ1 = floor(map(j1.posX + (TAILLE_CASE / 2), 0, TAILLE_FENETRE, 0, NB_CASES));
		// Si joueur 1 est dans une case vide, on la change d'état, sinon, la partie est perdu
		if (0 <= ligneJ1 && ligneJ1 < NB_CASES && 0 <= colonneJ1 && colonneJ1 < NB_CASES
				&& etatCase[ligneJ1][colonneJ1] == 0) {
			etatCase[ligneJ1][colonneJ1] = 1;
		} else {
			gagnant += 1;
		}

		// On défini sur quelle case se trouve le joueur 2
		int ligneJ2 = floor(map(j2.posY + (TAILLE_CASE / 2), 0, TAILLE_FENETRE, 0, NB_CASES));
		int colonneJ2 = floor(map(j2.posX + (TAILLE_CASE / 2), 0, TAILLE_FENETRE, 0, NB_CASES));
		// Si joueur 2 est dans une case vide, on la change d'état, sinon, la partie est perdu
		if (0 <= ligneJ2 && ligneJ2 < NB_CASES && 0 <= colonneJ2 && colonneJ2 < NB_CASES
				&& etatCase[ligneJ2][colonneJ2] == 0) {
			etatCase[ligneJ2][colonneJ2] = 2;
		} else {
			gagnant += 2;
		}
		gameOver();

	}

	public void gameOver() {
		// Fonction qui met fin à la partie
		if (gagnant != 0) {
			fill(0, 0, 0, 125);
			rect(0, 0, TAILLE_FENETRE, TAILLE_FENETRE);
			textSize(50);
			textAlign(CENTER);
			fill(255);
			switch (gagnant) {
			case 1:
				text(j2.nom + " a gagné !", width / 2, height / 2);
				break;
			case 2:
				text(j1.nom + " a gagné !", width / 2, height / 2);
				break;
			default:
				text("Match nul !", width / 2, height / 2);
				break;
			}
			noLoop();
		}
	}

	public static void main(String[] args) {
		PApplet.main("tron.Fenetre");
	}

}
