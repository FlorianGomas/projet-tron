package tron;

import processing.core.PApplet;

public class Joueur {

	float posX, posY;
	int hauteur, largeur;
	int couleur;
	String nom;
	PApplet Fenetre;

	// Constructeur
	Joueur(PApplet F, float posX, float posY, int hauteur, int largeur, int couleur, String nom) {

		Fenetre = F;
		this.posX = posX;
		this.posY = posY;
		this.hauteur = hauteur;
		this.largeur = largeur;
		this.couleur = couleur;
		this.nom = nom;
	}

	// Dessiner les joueurs
	void dessiner() {
		Fenetre.fill(couleur);
		Fenetre.noStroke();
		Fenetre.rect(posX, posY, hauteur, largeur);
	}

}
